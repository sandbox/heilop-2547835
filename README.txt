
-- SUMMARY --
This module allows to use Unite Gallery plugin.

About Unite Gallery

The Unite Gallery is multipurpose javascript gallery based on jquery library.
It's built with a modular technique with a lot of accent of ease of use and customization.
It's very easy to customize the gallery, changing it's skin via css, and even writing your own theme.
Yet this gallery is very powerfull, fast and has the most of nowdays must have
features like responsiveness, touch enabled and even zoom feature, it's unique effect.

Features / [To do]

- The gallery plays VIDEO from: Youtube, Vimeo, HTML5, Wistia and SoundCloud (not a video but still )
- Responsive - fits to every screen with automatic ratio preserve
- Touch Enabled - Every gallery parts can be controlled by the touch on touch enabled devices
- Responsive - The gallery can fit every screen size, and can respond to a screen size change.
- Skinnable - Allow to change skin with ease in different css file without touching main gallery css.
- Themable - The gallery has various of themes, each theme has it's own options and features, but it uses gallery core objects
- Zoom Effect - The gallery has unique zoom effect that could be applied within buttons, mouse wheel or pinch gesture on touch - enabled devices
- Gallery Buttons - The gallery has buttons on it, like full screen or play/pause that optimized for touch devidces access
- Keyboard controls - The gallery could be controlled by keyboard (left, right arrows)
- Tons of options. The gallery has huge amount of options for every gallery object that make the customization process easy and fun.
- Powerfull API - using the gallery API you can integrate the gallery into your website behaviour and use it with another items like lightboxes etc.


-- REQUIREMENTS --

This module uses the Unite Gallery Plugin and have dependency on
libraries modules and jQuery Update module.


-- INSTALLATION --

* Place the entire Unite Gallery directory into your Drupal sites
	modules directory (eg sites/all/modules).
* Enable this module by navigating to: Administration > Modules.
* Download and enable the libraries module.
* Download jQuery Unite Gallery Library(version 1.7.14) from
	https://github.com/vvvmax/unitegallery/archive/master.zip
* Install it in sites/all/libraries directory. Move only unitegallery
folder located on unitegallery-master/package/ from extracted files.
* The library should be available at a path like
  sites/all/libraries/unitegallery/js/unitegallery.js


-- CONFIGURATION --
